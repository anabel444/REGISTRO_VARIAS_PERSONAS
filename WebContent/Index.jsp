<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Bienvenida</title>
</head>
<body>

	<%@page import="eus.zornotza.Persona"%>
	<%@page import="java.util.*"%>

	Bienvenido &nbsp;&nbsp;
	<%
	/* La primera vez que se carga el fichero, el atributo */
	ArrayList<Persona> lista = (ArrayList<Persona>) session.getAttribute("lista_sesion"); 
		if (lista == null) { /* la primera vez la lista esta vacia */
			lista = new ArrayList<Persona>();
			session.setAttribute("lista_sesion", lista);
		}
		String nombre = request.getParameter("nombre");
		String apellido = request.getParameter("apellido");
		String sexo = request.getParameter("sexo");
		String direccion = request.getParameter("direccion");
		String poblacion = request.getParameter("poblacion");
		String valor = "";

		
		out.println("<for/>");
		if ((nombre != null) && (apellido != null) && (sexo != null) && (direccion != null)
				&& (poblacion != null)) {
			if (sexo.equals("H")) {
				valor = "Hombre";
			} else if (sexo.equals("M")) {
				valor = "Mujer";
			}
			out.println("<h2>" + "Persona registrada" + "</h2>");
			Persona persona=new Persona(nombre,apellido,valor,direccion,poblacion);
			lista.add(persona);
		} else {
			out.println("<h2>" + "Rellena todos los campos" + "</h2>");
		}
		for (Persona persona : lista) {
			out.println("Eres " + persona.getNombre() + " " + persona.getApellido() + " tu sexo es " + persona.getSexo() + " tu direccion es " + persona.getDireccion()
					+ " de " + persona.getPoblacion());
		}
	%>

</body>
</html>